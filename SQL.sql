--See on lihtne hello world teksti p�ring, mis tagastab �he rea ja �he veeru
--(veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World.

SELECT'Hello World';


--T�isarvu saab k�sida ilma 
SELECT 3;

--Ei t��ta PostgreSQL,is. N�iteks MSSql'is t��tab.
SELECT 'toomas'+ 'k�is' + 'joomas';


--Standard CONCAT t��tab k�igis erinevates SQL serverites.
SELECT CONCAT ('toomas ', 'k�is ', 'joomas',' kell', ' 2' );


--Kui tahan erinevaid veerge, siis panen v��rtustele koma, vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'


--AS m�rks�naga saab anda antud veerule nime
SELECT 
	'Peeter' AS Eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus,
	75.45 As pikkus,
	'Blond' AS juuksev�rv
	
--N�itab praegust kellaaega, kuup�eva, mingis vaikimisi formaadis.
SELECT NOW();
--Kui tahan konkreetset osa sellest, nt aastat v kuud, siis:

SELECT date_part('year',NOW())
SELECT 
	'Peeter' AS Eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus,
	75.45 As pikkus,
	'Blond' AS juuksev�rv,
	NOW() AS kuup�ev;
	
--Kuup�eva formaatimine Eesti kuup�eva formaati.	
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')	
	
	